/*
 * Labo 7 - Tours de Hanoï
 * Auteurs : Gil Balsiger & Julien Béguin
 * Fichier : HanoiDisplayer.java
 * Date    : 01.12.19
 */

package hanoi;

/**
 * Classe fournissant la fonction d'affichage dans le terminal pour la classe Hanoi
 */
public class HanoiDisplayer {

    /**
     * Fonction d'affichage dans le terminal de la classe Hanoi
     * @param h Instance de la classe Hanoi
     */
    public void display(Hanoi h) {
        System.out.println(h);
    }
}
