/*
 * Labo 7 - Tours de Hanoï
 * Auteurs : Gil Balsiger & Julien Béguin
 * Fichier : Hanoi.java
 * Date    : 01.12.19
 */

package hanoi;

import hanoi.gui.JHanoi;
import util.Iterator;
import util.Stack;

/**
 * Classe permettant de contenir et de résoudre le problème des tours de Hanoi
 */
public class Hanoi {
    private final static int RODS_NB = 3;
    private HanoiDisplayer displayer;
    private Stack[] rods = new Stack[RODS_NB];
    private int turn;
    private int disks;

    /**
     * Constructeur de la classe Hanoi
     * @param disks Nombre de disques
     * @param displayer Méthode d'affichage (terminal ou GUI)
     */
    public Hanoi(int disks, HanoiDisplayer displayer) {
        this.displayer = displayer;
        this.disks = disks;
        for (int i = 0; i < rods.length; ++i) {
            rods[i] = new Stack();
        }
        for (int i = disks; i > 0; --i) {
            rods[0].push(i);
        }
    }

    /**
     * Constructeur de la classe Hanoi
     * @param disks Nombre de disques
     */
    public Hanoi(int disks) {
        this(disks, new HanoiDisplayer());
    }

    /**
     * Initialisation de la résolution du problème des tours de Hanoi
     */
    public void solve() {
        displayer.display(this);
        recursifSolve(rods[0].getSize(), rods[0], rods[1], rods[2]);
    }

    /**
     * Récursion de la résolution du problème des tours de Hanoi
     * @param n Nombre de disques
     * @param from Aiguille de départ
     * @param intermediate Aiguille intermediare
     * @param to Aiguille d'arrivé
     */
    private void recursifSolve(int n, Stack from, Stack intermediate, Stack to) {
        if (n != 1) {
            recursifSolve(n - 1, from, to, intermediate);
            to.push(from.pop());
            turn++;
            displayer.display(this);
            recursifSolve(n - 1, intermediate, from, to);
        } else {
            to.push(from.pop());
            turn++;
            displayer.display(this);
        }
    }

    /**
     * Récupère le contenu des aiguilles
     * @return statut des aiguilles
     */
    public int[][] status() {
        // Etat initial
        int[][] state = new int[rods.length][];

        // Pour chaque aiguilles
        for (int i = 0; i < rods.length; i++) {
            int[] tmp = new int[rods[i].getSize()];
            Iterator it = rods[i].iterator();

            // Récupère les disques de l'aiguille actuelle
            for (int j = 0; j < tmp.length; j++) {
                tmp[j] = (int)it.next();
            }
            state[i] = tmp;
        }
        
        return state;
    }

    /**
     * Indique si le problème des tours de Hanoi est résolu
     * @return true si le problème est résolu
     */
    public boolean finished() {
        return (
                rods[0].getSize() == 0 &&
                rods[1].getSize() == 0 &&
                rods[2].getSize() == disks
        );
    }

    /**
     * @return Le nombre actuel de tour de la résolution du problème de Hanoi
     */
    public int turn() {
        return turn;
    }

    /**
     * Affichage le l'état actuel de la structure (nombre de tour et position des disques sur les aiguilles)
     * @return String de l'état actuel
     */
    @Override
    public String toString() {
        return "-- Turn: " + turn() + "\n" +
                "One:\t" + rods[0] + "\n" +
                "Two:\t" + rods[1] + "\n" +
                "Three:\t" + rods[2];
    }

    public static void main(String[] args) {
        if(args.length == 1) {
            Hanoi h = new Hanoi(Integer.parseInt(args[0]));
            h.solve();
        } else {
            JHanoi jh = new JHanoi();
        }
    }
}