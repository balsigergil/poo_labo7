/*
 * Labo 7 - Tours de Hanoï
 * Auteurs : Gil Balsiger & Julien Béguin
 * Fichier : TestHanoi.java
 * Date    : 01.12.19
 */

import hanoi.Hanoi;
import hanoi.HanoiDisplayer;
import hanoi.gui.JHanoi;
import util.Iterator;
import util.Stack;

public class TestHanoi {
    public static void main(String[] args) {
        System.out.println("----- Test Stack -----");
        Stack s = new Stack();
        s.push(4);
        s.push(3);
        s.push(2);
        s.push(1);
        s.push(5);
        s.pop();

        System.out.println("État de la pile : " + s);

        System.out.println("\nParcours de la pile:");
        Object[] data = s.data();
        for (Object el : data)
            System.out.println(el + " ");

        System.out.println("\nTest avec iterator:");

        Iterator it = s.iterator();
        while(it.hasNext()) {
            Object el = it.next();
            System.out.println(el + "");
        }

        System.out.println("\n----- Test hanoi.Hanoi -----");
        Hanoi h = new Hanoi(4);
        h.solve();

        JHanoi jh = new JHanoi();
    }
}
