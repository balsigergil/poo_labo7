/*
 * Labo 7 - Tours de Hanoï
 * Auteurs : Gil Balsiger & Julien Béguin
 * Fichier : Stack.java
 * Date    : 01.12.19
 */

package util;

/**
 * Structure de donnée permettant la gestion d'un pile d'entier
 *
 * @author Gil Balsiger
 * @author Julien Béguin
 */
public class Stack {

    /**
     * Données de la stack
     */
    private Element top;

    /**
     * Index vers le prochain espace vide dans la stack
     */
    private int size;

    /**
     * Création d'une nouvelle pile vide
     */
    public Stack() {
        top = null;
        size = 0;
    }

    /**
     * Ajoute un élément sur le sommet de la pile
     *
     * @param element Élément à ajouter
     */
    public void push(Object element) {
        top = new Element(top, element);
        size++;
    }

    /**
     * Retourne le premier objet de la pile sans le supprimer
     *
     * @throws IllegalStateException Si la pile est vide
     * @return Premier objet de la pile
     */
    public Object top() {
        if (size > 0) {
            return top.data;
        } else {
            throw new IllegalStateException("Empty stack!");
        }
    }

    /**
     * Enlève le premier élément de la pile
     *
     * @throws IllegalStateException Si la pile est vide
     * @return Élément supprimé de la pile
     */
    public Object pop() {
        if (size > 0) {
            Object data = top.data;
            top = top.next;
            size--;
            return data;
        } else {
            throw new IllegalStateException("Empty stack!");
        }
    }

    /**
     * Retourne un tableau avec comme premier élément le sommet de la pile
     *
     * @return Tableau conteant les éléments de la pile
     */
    public Object[] data() {
        Object[] data = new Object[size];
        int i = 0;
        Iterator it = iterator();
        while (it.hasNext()) {
            data[i] = it.next();
            i++;
        }
        return data;
    }

    /**
     * @return Un nouvel itérateur sur la pile
     */
    public Iterator iterator() {
        return new Stack.Itr();
    }

    @Override
    public String toString() {
        if(size == 0) {
            return "[ ]";
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("[ ");
            Iterator it = iterator();
            do {
                Object o = it.next();
                sb.append("<");
                sb.append(o);
                sb.append(">");
                if (it.hasNext())
                    sb.append(" ");
            } while (it.hasNext());
            sb.append(" ]");
            return sb.toString();
        }
    }

    public int getSize() {
        return size;
    }

    /**
     * Classe interne privée gérant un élément de la pile dans une liste chaînée
     *
     * @author Gil Balsiger
     * @author Julien Béguin
     */
    private class Element {
        Element next;
        Object data;

        Element(Element next, Object data) {
            this.next = next;
            this.data = data;
        }

        @Override
        public String toString() {
            return data.toString();
        }
    }

    /**
     * Classe interne privée implémentant un itérateur sur la pile
     *
     * @author Gil Balsiger
     * @author Julien Béguin
     */
    private class Itr implements Iterator {

        /**
         * Cursteur sur l'élément courant
         */
        private Element cursor = top;

        /**
         * Si vrai, c'est qu'on itère sur le premier élément de la pile
         */
        private boolean first = true;

        /**
         * @return Est-ce qu'il reste un élément à parcourir
         */
        @Override
        public boolean hasNext() {
            return cursor != null && cursor.next != null;
        }

        /**
         * Retourne le prochain élément de la pile à moins que ce ne soit le premier élément
         * au quel cas on retourne la donnée au sommet
         *
         * @throws IllegalStateException Si il n'y a plus d'élément à parcourir
         * @return Prochain élément en commençant par le haut de la pile
         */
        @Override
        public Object next() {
            if (first && cursor != null) {
                first = false;
                return cursor.data;
            } else if(hasNext()) {
                cursor = cursor.next;
                return cursor.data;
            } else {
                throw new IllegalStateException("Stack has been fully browsed, no more elements!");
            }
        }
    }
}
