/*
 * Labo 7 - Tours de Hanoï
 * Auteurs : Gil Balsiger & Julien Béguin
 * Fichier : Iterator.java
 * Date    : 01.12.19
 */

package util;

/**
 * Itérateur sur la pile
 *
 * @author Gil Balsiger
 * @author Julien Béguin
 */
public interface Iterator {

    /**
     * Retourne le prochain élément de la pile
     * @return Prochain élément en commençant par le haut de la pile
     */
    Object next();

    /**
     * @return Est-ce qu'il reste un élément à parcourir
     */
    boolean hasNext();

}
